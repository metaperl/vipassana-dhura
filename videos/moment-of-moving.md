# Background

Edited from a dhamma talk by Achan Sobin Namto on October 13, 2001 in Littleton, CO.

# Talk

How can we understand what Lord Buddha taught? He didn't teach anything we don't have.
Five things we have:
1. Body: the Pali word is Rupa.
2. We have vedana, meaning feeling (pleasant, unpleasant, neutral).
3. Memory (Sanna).
4. Mental formations (Sankhara).
5. Consciousness (vinnana).

Only 5 things: rupa, vedana, sanna, sankhara and vinnana. Everybody
has the same - Thai people, Americans, etc.

After he grew up, he tried to run away from the palace. He became a
monk by himself, to look for the way to destroy defilments to get
wisdom, to understand the truth, to destroy ignorance in his mind
until he became free.

Many teachers at that time had great psychic power. The Buddha tried
to learn wisdom from them - but all taught only Samatha or power from
concentration, which can't destroy defilements. No one could teach him
how to develop wisdom to get enlightenment.

That's why, finally he practiced by himself, observing: "Why did my
teacher say I'm enlightened? Why do I still have doubt? How come I'm
still confused? I don't understand anything correctly. What can I do?"
Only, bring emotion, confusion and doubt in his own mind to become the
object. He used mindfulness to observe to see: this is the object, and
the one who knows the object. They become moment to moment. He brought
intention to observe the doubt itself - after that, it was less.

When confusing appeared, he bought intention to observe it. The
confusion disappared. "Before that, I thoguht confusion, doubt or
emotion were permanent. Now, as soon as I observe anything, it
disappears." He awoke to understand: "that's arising and passing away
of emotion or mental object. Even the one who knows the object arises
and passes away along with the object." Until he awakened to
understand impermanence of the moment to mement, and suffering, which
means: unsatisfacctory, unstable, and nonself: emotion doesn't belong
to anyone, it only belongs to the universe, belong to the truth,
belongs to cause and effect. That's why, don't think your emotions are
you.

## Dont become an actor

When appearing with you, frustration, anxiety or sadness, separate
yourself from the attachement - watch the emotions as watching actors
onstage. You have to regard the emotions in your mind as actors. What
do they show? Sometimes excitement, happiness; sometimes show bad
emotions, frustration or sadness or sorrow.

But remind yourself, "They're not me." Just watch them: "How far do
you go? How long do you stay?" If you don't get involved, don't
attach, how can you suffer? No way, when you don't attach, when you
don't think they belong to you. Only just know them, see, watch them
as watching actors.

When they finish their duty to show, they're gone, but they come back
to show another feeling. But it's only showing; it's not true. They
change all the time. (8:31) The Buddha taught that people think everything
belongs to them:
* rupa, belong to them
* feeling, belong to them
* memory, belong to them
* mental formations (e.g. emotions), belong to them
* consciousness, belong to them

E.g.: I am seeing, I am liking, disliking, hearing, smelling, tasting,
I am feeling. People think everything belongs to them because they
attach, but in truth these 5 things don't belong to anyone. Even the
body does not belong to me our you. Only, we use the body to spend the
result of karma. Sometimes we're sick or have some difficulty. That
comes from the result of karma. It doesn't depend on your expectation
to get something good or to be happy. Just say, "I'm spending the
result of karma already. I'm giving back what I did in the past" That
makes life more stable. If you feel bad, you suffer more. Because you
can't see the actors, you become an actor yourself.

(10:15) No one can watch the show when everyone is onstage. You have to have
someone watching the show. The one who sees can know, "there's a good
actor or bad." When something happens in life, don't become an actor,
don't feel bad, frustrated or disappointed. Just be happy to get the
result from karma to know, "that's from me. I made the karma before. I
will let it go."

(11:13)
No one can expect to see a beautiful picture all the time, impossible.
To hear a good voice all the time, impossible. To smell a good
fragrance, all the time, no way (chuckles). To have good food all the
time, no. It depends on what you've done in the past. If you've done
good things in the past, then you will experience more good things in the
present than bad. It simply is the cause and effect from karma in the
past. (12:12) For example, when we see something bad, don't feel
hatred. Accept it. Remind yourself, "I'm supposed to see that,
because that's my karma. I can't avoid that, but I won't hate it. I
won't want to make it go away. I have to observe and know I've spent
the result already." That's how to stop creating more karma. When
someone talks badly to you, just observe: "That's my karma, maybe I
spoke badly to them before, in a past life. Why do I have to hear bad
words. Because of me, not them. Because my ear's supposed to hear
that. I know that's my karma vipaka from a past period of life, so I
won't give bad back.  "  I won't do something bad to continue the
process of karma, to create more suffering." That's hope to stop
incarnation, to stop rebirth in samsara and having to experience the
result of what we did in the past. That's what the Buddha taught.

# (14:03) From 5 to 2: Rupa and Nama
